import { Component, OnInit } from '@angular/core';
import {ApiService } from 'src/app/services/api.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
   //constructor
  constructor(
    public api:ApiService,
    public router:Router
  ) { }

  ngOnInit(): void {
  }
//from validation
email = new FormControl('',[Validators.required,Validators.email]);
password=new FormControl('',[Validators.minLength(6),Validators.required]);
//register
loading:boolean | undefined;
register(user: { email: any; password1: any; })
{
  this.loading=true;
  this.api.register(user.email,user.password1).subscribe((res: any)=>{
    this.loading=false;
    alert('Register berhasil');

    this.router.navigate(['auth/login']);
    },()=>{
      this.loading=false;
      alert('Ada Masalah..');

  })
}
}
