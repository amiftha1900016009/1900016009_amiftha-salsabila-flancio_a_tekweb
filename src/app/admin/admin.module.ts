import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { materialDesign } from '../material/material.module';
import { ImageComponent } from './image/image.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path:'',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path:'product',
        component:ProductComponent,
      },
      {
        path:'image',
        component:ImageComponent
      }
    ]
  }
]



@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ImageComponent,
    ProductComponent,
    ProductDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    materialDesign,
    FormsModule,
    HttpClientModule
  ]
})
export class AdminModule { }
