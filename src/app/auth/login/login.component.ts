import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any = {};
  constructor(
    public api: ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  email = new FormControl('', [Validators.required,
  Validators.email]);
  password = new FormControl('', [Validators.required]);

  loading:Boolean | undefined;
  login(user: any)
  {
    this.loading=true;
    this.api.login(this.user.email, this.user.password).subscribe((res: any)=>{
      this.loading=false;
      localStorage.setItem('appToken',JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    },(err: any)=>{
      this.loading=false;
      alert('tidak dapat login');
  });
 }

}

